<?php

namespace Src\System;


class DatabaseConnector
{
    private $dbConnection = null;


    public function __construct()
    {
        //0.0.0.0 3306 db_user db_password
        $host = getenv('DB_HOST');
        $port = getenv('DB_PORT');
        $db = getenv('DB_DATABASE');
        $user = getenv('DB_USERNAME');
        $pass = getenv('DB_PASSWORD');

        try {
//            $this->dbConnection = new \PDO('mysql:host=db;port=3306;dbname=library','root','your_mysql_root_password');
            $this->dbConnection = new \PDO(
                "mysql:host=$host;
                port=$port;
                dbname=$db",
                $user,
                $pass);
//            echo "connecting Success!\n";

        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function getConnection()
    {
        return $this->dbConnection;
    }
}