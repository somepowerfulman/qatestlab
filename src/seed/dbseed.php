<?php
require dirname(__DIR__).'/../bootstrap.php';

$authors = <<<EOS
drop table author_books;
drop table books;
drop table authors;
drop table publishers;


CREATE TABLE IF NOT EXISTS publishers (
id SMALLINT auto_increment not null primary key,
title_publisher varchar(128) NOT NULL);

CREATE TABLE IF NOT EXISTS authors (
id SMALLINT auto_increment not null primary key,
name varchar(128) NOT NULL);

CREATE TABLE IF NOT EXISTS books (
id SMALLINT auto_increment not null primary key,
title varchar(128) NOT NULL,
publisher_id SMALLINT not null,
foreign key (publisher_id) references publishers(id));


CREATE TABLE IF NOT EXISTS author_books(
id SMALLINT  auto_increment not null primary key,
author_id SMALLINT not null,
book_id SMALLINT not null,
foreign key (author_id) references authors(id),
foreign key (book_id) references books(id)
);

INSERT INTO authors
(name)
VALUES
('Krasimir'),
('WILLIAM FAULKNER'),
('Masha'),
('Jane'),
('John'),
('Richard'),
('Donna'),
('Josh'),
('Anna');

INSERT INTO publishers
(title_publisher)
VALUES
('Penguin Random House'),
('Hachette Livre'),
('HarperCollins'),
('Macmillan Publishers'),
('Simon & Schuster'),
('McGraw-Hill Education');

INSERT INTO books
(title, publisher_id)
values
('A Woman of No Importance',4),
('The Vietnam War',2),
('The Glass Hotel',6),
('ABSALOM, ABSALOM!',5),
('Do Androids Dream of Electric Sheep?',4),
('Something Wicked This Way Comes',3),
('Pride and Prejudice and Zombies (Pride and Prejudice and Zombies',3),
('The Curious Incident of the Dog in the Night-Time',2),
('I Was Told There''d Be Cake',1),
('To Kill a Mockingbird',4);


insert into author_books
(author_id, book_id)
values
(1,1),
(3,1),
(2,2),
(3,3),
(6,3),
(1,4),
(2,4),
(1,5),
(5,6),
(2,7),
(6,8),
(2,8),
(7,9),
(8,10);
EOS;

try {
    $createAuthors = $dbConnection ->exec($authors);
    echo "Success!\n";
}
catch (\PDOException $e) {
    exit($e->getMessage());
}
