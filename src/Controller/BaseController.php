<?php


namespace Src\Controller;


abstract class BaseController
{
    private $dbConnection;
    private $requestMethod;
    private $id;

    private $model;


    public function __construct($dbConnection, $id, $requestMethod)
    {
        $this->dbConnection = $dbConnection;
        $this->requestMethod = $requestMethod;
        $this->id = $id;
        $this->model = $this->getModel($dbConnection);
    }

    abstract protected function getModel($dbConnection);

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                if ($this->id) {
                    $response = $this->getItem($this->id);
                } else {
                    $page_size = isset($_GET['page_size']) ? $_GET['page_size'] : 25;
                    $page_number = isset($_GET['page_number']) ? $_GET['page_number'] : 1;
                    $response = $this->getAll($page_size, $page_number);
                };
                break;
            case 'POST':
                $response = $this->createItemFromRequest();
                break;
            case 'PUT':
                $response = $this->updateFromRequest($this->id);
                break;
            case 'DELETE':
                $response = $this->deleteFromRequest($this->id);
                break;

            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];

        }


    }

    private function getAll(int $page_size, int $page_number)
    {
        if ($page_number == 0) {
            $offset = $page_number;
            }else{
            $offset = ($page_size * $page_number);
        }
        $page_s = $page_size+1;
        $result = $this->model->findAll($offset, $page_s);
        $has_next = ($page_s === count($result) ? true :false);
        $result = array_slice($result, 0,$page_size);
        $links = $this->getLinksPagination($page_size, $page_number, $has_next);
        array_push($result,  $links);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }


    private function getLinksPagination(int $page_size, int $page_number, bool $has_next)
    {
        $np = $page_number+1;
        $pp = $page_number-1;
        $uri = trim($_SERVER['REQUEST_URI'], '/');
        $uri = explode('?', $uri);
        $has_next === true ? $next = '/'.$uri[0]."?page_size=$page_size&page_number=".$np : null;
        $prev = $page_number >0 ? $prev = '/'.$uri[0]."?page_size=$page_size&page_number=".$pp: null;
        $links = array("links",['prev' =>$prev, 'next'=>$next]);

        return $links;
    }

    private function getItem($id)
    {

        $result = $this->model->findItem($id);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode($result);
        return $response;
    }

    private function createItemFromRequest()
    {
        $input = (array)json_decode(file_get_contents('php://input'), true);


        $data = $this->model->insert($input);
        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode(['response' => $data]);
        return $response;
    }

    private function updateFromRequest($id)
    {
        $result = $this->model->findItem($id);
        if (!$result) {
            return $this->notFoundResponse();
        }
        $input = (array)json_decode(file_get_contents('php://input'), true);

        $data = $this->model->update($id, $input);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode(['response' => $data]);
        return $response;
    }

    private function deleteFromRequest($id)
    {
        $result = $this->model->findItem($id);
        if (!$result) {

        }
        $data = $this->model->delete($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        if ($data === true) {
            $response['body'] = json_encode(['response' => $data]);
        } else {
            $response['body'] = json_encode(['response' => 'id not found, or referenced']);
        }
        return $response;

    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = json_encode(['response' => 'ERROR 404']);
        return $response;
    }

}