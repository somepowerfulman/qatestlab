<?php


namespace Src\Controller;


use Src\Model\PublishersModel;

class PublishersController extends BaseController
{
    protected function getModel($dbConnection)
    {
        return new PublishersModel($dbConnection);
    }
}