<?php


namespace Src\Controller;


use Src\Model\BooksModel;

class BooksController extends BaseController
{

    protected function getModel($dbConnection)
    {
        return new BooksModel($dbConnection);
    }

}