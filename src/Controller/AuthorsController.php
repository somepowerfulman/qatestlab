<?php


namespace Src\Controller;


use Src\Model\AuthorsModel;

class AuthorsController extends BaseController
{
    protected function getModel($dbConnection)
    {
        return new AuthorsModel($dbConnection);
    }
}