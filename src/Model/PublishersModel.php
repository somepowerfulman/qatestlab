<?php


namespace Src\Model;


class PublishersModel extends BaseModel
{
    protected $tableName = 'publishers';


    public function prepareInsertData(array $input): array
    {
        return array($this->tableName, [['title_publisher' => $input['title_publisher']]]);
    }

    protected function getFindAllQuery($page_size, $page_number)
    {

        return "SELECT  publishers.id, publishers.title_publisher
                FROM publishers group by publishers.id asc LIMIT $page_size OFFSET $page_number;";
    }

    protected function getFindItemQuery()
    {
        return 'SELECT  publishers.id, publishers.title_publisher
                FROM publishers where publishers.id=?;';
    }

}