<?php


namespace Src\Model;


abstract class BaseModel
{
    private $db = null;
    protected $tableName = 'fooBar';

    public function __construct($db)
    {
        $this->db = $db;
    }

    function pdoMultiInsert($tableName, $data, $pdoObject)
    {

        //Will contain SQL snippets.
        $rowsSQL = array();

        //Will contain the values that we need to bind.
        $toBind = array();

        //Get a list of column names to use in the SQL statement.
        $columnNames = array_keys($data[0]);

        //Loop through our $data array.
        foreach ($data as $arrayIndex => $row) {
            $params = array();
            foreach ($row as $columnName => $columnValue) {
                $param = ":" . $columnName . $arrayIndex;
                $params[] = $param;
                $toBind[$param] = $columnValue;
            }
            $rowsSQL[] = "(" . implode(", ", $params) . ")";
        }

        //Construct our SQL statement
        $sql = "INSERT INTO `$tableName` (" . implode(", ", $columnNames) . ") VALUES " . implode(", ", $rowsSQL);

        //Prepare our PDO statement.
        $pdoStatement = $pdoObject->prepare($sql);

        //Bind our values.
        foreach ($toBind as $param => $val) {
            $pdoStatement->bindValue($param, $val);
        }

        //Execute our statement (i.e. insert the data).
        return $pdoStatement->execute();
    }

    public function getDb()
    {
        return $this->db;
    }

    abstract protected function getFindAllQuery($page_size, $page_number);

    abstract protected function getFindItemQuery();

    public function runQuery(string $statement, array $query_args = null)
    {

        try {
            $statement = $this->getDb()->prepare($statement);
            $statement->execute($query_args);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findAll($offset, $page_size)
    {
        $limit = $page_size;
        $statement = $this->getFindAllQuery($limit, $offset);
        $result = $this->runQuery($statement);
        return $this->prepare_result($result);
    }

    public function findItem($id)
    {
        $statement = $this->getFindItemQuery();
        $result = $this->runQuery($statement, array($id));
        return $this->prepare_result($result);
    }

    protected function prepare_result($result)
    {
        return $result;
    }


    public function prepareInsertData(array $input): array
    {

    }

    public function insert(array $input)
    {
        $prepared_data = $this->prepareInsertData($input);
        try {
            $this->getDb()->beginTransaction();
            $this->pdoMultiInsert($prepared_data[0], $prepared_data[1], $this->getDb());

            $item_id = $this->getDb()->lastInsertId();
            $this->getDb()->commit();
        } catch (\PDOException $e) {
            $this->getDb()->rollBack();
            exit($e->getMessage());
        }
        return $this->findItem($item_id);
    }

    public function update($id, array $input)
    {
        try {
            $this->getDb()->beginTransaction();
            $this->multiUpdateData($id, $input, $this->getDb());
            $this->getDb()->commit();
        } catch (\PDOException $e) {
            $this->getDb()->rollBack();
            exit($e->getMessage());
        }
        return $this->findItem($id);
    }

    private function multiUpdateData($id, array $input, $pdoObject)
    {
        $table = strtolower(substr((substr(strrchr(get_class($this), "\\"), 1)), 0, -5));
        $input_books = $input;
        if (!array_key_exists('author_id', $input_books)) {
            $sql = $this->prepareToUpdateSQL($id, $table, $input_books);
            $sql = $pdoObject->query($sql);
            return $sql;

        } else {
            unset($input_books['author_id']);
            $update_books = $this->prepareToUpdateSQL($id, $table, $input_books);
            $del_references_id = "delete from author_books where book_id = $id;";
            $references_id = $this->getDb()->prepare($del_references_id);
            $references_id = $references_id->execute();
            $data = [];
            foreach ($input['author_id'] as $item) {
                $data [] = [
                    'author_id' => $item,
                    'book_id' => $id
                ];
            }
            $this->pdoMultiInsert('author_books', $data, $this->getDb());
            $sql = $this->prepareToUpdateSQL($id, $table, $input_books);
            $sql = $pdoObject->query($sql);
            return $sql;
        }

    }

    function prepareToUpdateSQL($id, $table, array $input)
    {
        $pieces = array_map(function ($key, $value) {
            return sprintf("%s='%s'", $key, $value);
        }, array_keys($input), $input);

        $kw_args = implode(', ', $pieces);
        $sql = "UPDATE $table SET $kw_args WHERE id=$id";
        return $sql;
    }

    public function delete($id)
    {
        try {
            $this->getDb()->beginTransaction();
            foreach ($this->prepare_delete_data($id) as $record) {
                [$table, $kwargs] = $record;
                $kw_args = implode(" ", array_map(
                        function ($key, $value) {
                            return sprintf("%s=%s", $key, $value);
                        },
                        array_keys($kwargs), $kwargs)
                );
                $sql = "DELETE FROM $table WHERE $kw_args;";
                $sql = $this->getDb()->prepare($sql);
                $sql = $sql->execute();
            }
            $this->getDb()->commit();

        } catch (\PDOException $e) {
            $this->getDb()->rollBack();
            exit($e->getMessage());
        }
        return $sql;
    }

    protected function prepare_delete_data($id): array
    {
        return array(
            array($this->tableName, array('id' => $id))
        );
    }


}


