<?php


namespace Src\Model;


class AuthorsModel extends BaseModel
{
    protected $tableName = 'authors';

    protected function getFindAllQuery($page_size, $page_number){
        return "SELECT  authors.id, authors.name
                 FROM authors order by authors.id asc LIMIT $page_size OFFSET $page_number;";
    }
    
    protected function getFindItemQuery()
    {
       return 'SELECT  authors.id, authors.name
                FROM authors where authors.id=?;';
    }
    
    /**
     * @param array $input
     * @return array['table', array(fields)]
     */
    public function prepareInsertData(array $input): array
    {
        return array($this->tableName, [['name' => $input['name']]]);
    }

}