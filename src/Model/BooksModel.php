<?php


namespace Src\Model;


class BooksModel extends BaseModel
{
    protected $tableName = 'books';
    private $get_query = "select 
            books.id, 
            books.title,
            JSON_ARRAYAGG(JSON_OBJECT('author_id', authors.id, 'author_name', authors.name)) as authors,
            publishers.title_publisher
        from books 
        join publishers on books.publisher_id = publishers.id
        join author_books on author_books.book_id = books.id 
        join authors on authors.id = author_books.author_id
        %s
        group by books.id asc LIMIT %d OFFSET %d";

    public function getFindAllQuery($limit, $offset)
    {
//        var_dump(sprintf($this->get_query, '', $page_size, $page_number));
        return sprintf($this->get_query, '', $limit, $offset);
    }


    public function getFindItemQuery()
    {
        return sprintf($this->get_query, 'where books.id=?');
    }

    protected function prepare_result($result)
    {
        $result = parent::prepare_result($result);
        foreach ($result as $id => $record){
            $result[$id]['authors'] = json_decode($record['authors']);
        }

        return $result;
    }

    public function insert(array $input)
    {
        $book_data = [['title' => $input['title'], 'publisher_id' => $input['publisher_id']]];

        try {
            $this->getDb()->beginTransaction();
            $this->pdoMultiInsert($this->tableName, $book_data, $this->getDb());

            $book_id = $this->getDb()->lastInsertId();
            $data = [];
            foreach ($input['author_id'] as $item) {
                $data [] = [
                    'author_id' => $item,
                    'book_id' => $book_id
                ];
            }

            $this->pdoMultiInsert('author_books', $data, $this->getDb());
            $this->getDb()->commit();
        } catch (\PDOException $e) {
            $this->getDb()->rollBack();
            exit($e->getMessage());
        }
        return $this->findItem($book_id);
    }

    protected function prepare_delete_data($id): array
    {
        return array(
            array('author_books', array('book_id' => $id)),
            array($this->tableName, array('id' => $id))
        );
    }
}