
CREATE TABLE publishers (
id SMALLINT auto_increment not null primary key,
title varchar(128) NOT NULL);

CREATE TABLE authors (
id SMALLINT auto_increment not null primary key,
name varchar(128) NOT NULL
);

CREATE TABLE books (
id SMALLINT auto_increment not null primary key,
title varchar(128) NOT NULL,
amount SMALLINT  unsigned,
publisher_id SMALLINT NOT NULL,
foreign key (publisher_id) references publishers(id)
);

CREATE TABLE author_books(
id SMALLINT  auto_increment not null primary key,
author_id SMALLINT not null,
book_id SMALLINT not null,
foreign key (author_id) references books(id),
foreign key (book_id) references authors(id)
);

INSERT INTO authors
(name)
VALUES
('Krasimir'),
('WILLIAM FAULKNER'),
('Masha'),
('Jane'),
('John'),
('Richard'),
('Donna'),
('Josh'),
('Anna');

INSERT INTO publishers
(title)
VALUES
('Penguin Random House'),
('Hachette Livre'),
('HarperCollins'),
('Macmillan Publishers'),
('Simon & Schuster'),
('McGraw-Hill Education');

INSERT INTO books
(title, amount, publisher_id)
values
('A Woman of No Importance',8,1),
('The Vietnam War',7,1),
('The Glass Hotel',6,1),
('ABSALOM, ABSALOM!',5,2),
('Do Androids Dream of Electric Sheep?',4,2),
('Something Wicked This Way Comes',3,3),
('Pride and Prejudice and Zombies (Pride and Prejudice and Zombies',3,4),
('The Curious Incident of the Dog in the Night-Time',2,4),
('I Was Told There''d Be Cake',1,5),
('To Kill a Mockingbird',4,5);

insert into author_books
(author_id, book_id)
values
(1,1),
(3,1),
(2,2),
(3,3),
(6,3),
(1,4),
(2,4),
(1,5),
(5,6),
(2,7),
(6,8),
(2,8),
(7,9),
(8,10);