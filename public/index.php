<?php

use Src\Controller\AuthorsController;
use Src\Controller\BooksController;
use Src\Controller\PublishersController;
use Src\Model\BooksModel;

require dirname(__DIR__) . '/bootstrap.php';
require dirname(__DIR__) . '/src/init.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$uri = trim($_SERVER['REQUEST_URI'], '/');
$uri = explode('/', $uri);
if ($uri[0] !== 'api' and $uri[1] == 'v1') {
    header("HTTP/1.1 404 Not Found");
    exit();
}
if (isset($uri[3])) {
    $id = (int)$uri[3];

}

$requestMethod = $_SERVER["REQUEST_METHOD"];
$uri_param = explode('?',$uri[2]);
switch ($uri_param[0]) {
    case 'books':
        $controller = new BooksController($dbConnection, $id, $requestMethod);
        break;

    case 'authors':
        $controller = new AuthorsController($dbConnection, $id, $requestMethod);
        break;

    case 'publishers':
        $controller = new PublishersController($dbConnection, $id, $requestMethod);
        break;
}

$controller->processRequest();
