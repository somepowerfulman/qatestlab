#Start app, database, webserver
docker-compose up -d
#Configuring DB
docker-compose exec db bash
mysql -u root -p
your_mysql_root_password
CREATE DATABASE library CHARACTER SET utf8 COLLATE utf8_general_ci;
show databases;
-------------------------------------------------------------
GRANT ALL ON library.* TO 'db_user'@'%' IDENTIFIED BY 'db_password';
or:
-------------------------------------------------------------
CREATE USER 'db_user'@'%' identified by 'db_password';
GRANT ALL on library.* to 'db_user'@'localhost';
-------------------------------------------------------------
SELECT User,Host FROM mysql.user;
exit;
exit
#installing dependencies
composer install
#installing dependencies
#Seeding data run:
docker-compose exec app php src/seed/dbseed.php